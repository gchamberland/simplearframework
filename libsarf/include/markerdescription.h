#ifndef MARKER_DESCRIPTION_H
#define MARKER_DESCRIPTION_H

#include <cstddef>
#include <iostream>
#include <vector>

class MarkerDescription {
	public:
		virtual const void *getId() const = 0;
		virtual int isEqualsWithRotation(const void *id, size_t idLength) const = 0;
		virtual float getRealSizeInMeter() const = 0;
		virtual std::ostream& dump(std::ostream& o) const = 0;

		virtual size_t getIdLength() const = 0;
		virtual bool needThresholding() const = 0;
		virtual void readId(void *unwarppedMarkerImgData, const size_t &unwarpedImageSize, void *dst) const = 0;
		virtual void detectFeatures(void *unwarpedMarkerImgData, const size_t &unwarpedImageSize, std::vector<double> &features) const = 0;
};

inline std::ostream& operator<<(std::ostream& o, const MarkerDescription& md) { return md.dump(o); }

#endif