#ifndef OBB_MARKER
#define OBB_MARKER

#include "markerdescription.h"

class ORBMarker : public MarkerDescription {
	public:
		virtual const void *getId() const = 0;
		virtual int isEqualsWithRotation(const void *id, size_t idLength) const = 0;
		virtual float getRealSizeInMeter() const = 0;
		virtual std::ostream& dump(std::ostream& o) const = 0;

		virtual size_t getIdLength() const = 0;
		virtual void readId(void *unwarppedMarkerImgData, const size_t &unwarpedImageSize, void *dst) const = 0;
		virtual void detectedFeatures(void *unwarpedMarkerImgData, const size_t &unwarpedImageSize, std::vector<double> &features) const = 0;
};

#endif