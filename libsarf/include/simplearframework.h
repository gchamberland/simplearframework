#ifndef SIMPLE_ARFRAMEWORK_H
#define SIMPLE_ARFRAMEWORK_H

#include "markerdescription.h"
#include "bitmatrixmarker.h"
#include "orbmarker.h"
#include "markerposition.h"

#include "../src/edgeldetector.h"
#include "../src/linesegment.h"
#include "../src/buffer.h"

#include "opencv2/opencv.hpp"

//#include "../src/ippe.h"

#include "posesolver.h"

#include <typeinfo>
#include <typeindex>
#include <vector>
#include <algorithm>
#include <map>
#include <deque>

#include <iostream>

class Detector
{
	private:
		std::vector<const MarkerDescription*> knownMarkers;
		std::map<const MarkerDescription *, std::deque<std::pair<const MarkerPosition, int> > > detectedMarkers;
		std::map<std::type_index, std::vector<uint8_t> > idCache;
		std::map<std::type_index, bool> idCacheInfo;
		Buffer buffer;
		EdgelDetector edgelDetector;
		cv::Mat corners;
		cv::Mat unwarpedCorners;
		//IPPE::PoseSolver solver;
		PoseSolver *solver;
		cv::Mat markerPoints;
		cv::Mat markerPointsDouble;
		cv::Matx<double, 3, 3> cameraMatrix;
		cv::Matx<double, 5, 1> distCoefs;
		unsigned int frameIndex;

		cv::Vec4d zAxis;

		void init();
		std::map<const MarkerDescription *, const MarkerPosition> matchKnownMarkers(std::vector<ARMarker> &potentialMarkers, cv::Mat image);
		std::map<const MarkerDescription*, const MarkerPosition> linearizePoses(std::map<const MarkerDescription*, const MarkerPosition> &currentFrameMarkers);
		std::vector<ARMarker> findPotentialMarkers(const unsigned char *imageData, int width, int height);

	public:

		Detector(const double *cameraMatrixValuesRowMajor, const double *distCoefsValues, PoseSolver *solver = new IPPEGenericPoseSolver());
		void drawCorners(cv::Mat &dst, cv::Mat &corners);
		void registerMarker(const MarkerDescription *marker);
		std::map<const MarkerDescription *, const MarkerPosition> analyseImage(void *imageData, int width, int height);
		std::map<const MarkerDescription *, const MarkerPosition> analyseImage(void *imageData, int width, int height, std::vector<ARMarker> &portentialMarkers);
		std::map<const MarkerDescription *, const MarkerPosition> analyseImageLinear(void *imageData, int width, int height);
		std::map<const MarkerDescription *, const MarkerPosition> analyseImageLinear(void *imageData, int width, int height, std::vector<ARMarker> &potentialMarkers);
		void setCameraMatrix(const double *values);
		void setDistCoefs(const double *values);
};

#endif