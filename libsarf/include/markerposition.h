#ifndef MARKER_POSITION_H
#define MARKER_POSITION_H

#include <vector>

typedef struct {
	double transformation3d[16];
	double corners2d[8];
	std::vector<double> features2d;
} MarkerPosition;

#endif