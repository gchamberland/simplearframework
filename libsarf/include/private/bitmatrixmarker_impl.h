#ifndef BITMATRIX_MARKER_IMPL_H
#define BITMATRIX_MARKER_IMPL_H

#include <cstdlib>
#include <opencv2/opencv.hpp>
#include <vector>

class BitMatrixMarkerImpl
{
private:
    cv::Mat img;

public:
    void loadImage(const size_t &imageSize, void *imageData);
    int countNonZero(const size_t &x1, const size_t &y1, const size_t &x2,
                     const size_t &y2);
};

#endif