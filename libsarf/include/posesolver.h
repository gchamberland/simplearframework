#ifndef POSE_SOLVER_H
#define POSE_SOLVER_H

#include "markerposition.h"

class PoseSolver {
	public:
		virtual bool solveSquare(MarkerPosition &position, const float &squareSize, double *cameraMatrix, double *distCoefs) = 0;
};

class IPPESquarePoseSolver : public PoseSolver {
	public:
		virtual bool solveSquare(MarkerPosition &position, const float &squareSize, double *cameraMatrix, double *distCoefs);
};

class IPPEGenericPoseSolver : public PoseSolver {
	public:
		virtual bool solveSquare(MarkerPosition &position, const float &squareSize, double *cameraMatrix, double *distCoefs);
};

#endif