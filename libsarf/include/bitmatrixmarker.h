#ifndef BIT_MATRIX_MARKER_H
#define BIT_MATRIX_MARKER_H

#include "markerdescription.h"
#include "private/bitmatrixmarker_impl.h"

#include <cstddef>
#include <cstdint>
#include <iostream>
#include <bitset>

template <unsigned char MATRIX_SIZE>
class BitMatrixMarker : public MarkerDescription {
    using ID_TYPE = std::bitset<MATRIX_SIZE*MATRIX_SIZE>;
	private:
		ID_TYPE id[4];
		float realSizeInMeter, borderSizeInMeter;

		inline void setIdBit(size_t x, size_t y, size_t rotation) {
			id[rotation].set(y*MATRIX_SIZE+x, true);
		}

		inline bool getIdBit(size_t x, size_t y, size_t rotation) {
			return id[rotation][y*MATRIX_SIZE+x];
		}

		void rotateIdClockwise(size_t originalIndex, size_t rotatedIndex) {
			id[rotatedIndex] = ID_TYPE();
			for (int i=0; i<MATRIX_SIZE; i++) { 
				for (int j=0; j<MATRIX_SIZE; j++) {
					if (getIdBit(MATRIX_SIZE - j - 1, i, originalIndex)) {
						setIdBit(i, j, rotatedIndex);
					}
				}
			}
		}

		inline bool isIdEquals(const ID_TYPE &id, size_t rotation) const {
			//std::cout << (unsigned int) id << " = " << (unsigned int) this->id[rotation] << std::endl;
			return id == this->id[rotation];
		}

	public:
		BitMatrixMarker(const char* id, float realSizeInMeter, float borderSizeInMeter) {
            if (strnlen(id, 256) != MATRIX_SIZE*MATRIX_SIZE) {
                throw std::runtime_error("Invalid id !");
            }

			this->id[0] = ID_TYPE(id);
			rotateIdClockwise(0, 1);
			rotateIdClockwise(1, 2);
			rotateIdClockwise(2, 3);
			this->realSizeInMeter = realSizeInMeter;
			this->borderSizeInMeter = borderSizeInMeter;
		}

		virtual const void *getId() const {
			return id;
		}

		virtual int isEqualsWithRotation(const void *id, size_t idLength) const {
			if (idLength != sizeof(ID_TYPE)) {
				return -1;
			}

			ID_TYPE val = *((ID_TYPE *) id);

			for (int i=0; i<4; i++) {
				if (isIdEquals(val, i)) {
					return i;
				}
			}
			return -1;
		}

		virtual float getRealSizeInMeter() const {
			return this->realSizeInMeter;
		}

		virtual std::ostream& dump(std::ostream& o) const {
      		return o << "BitMatrixMarker(" << id[0] << ")";
  		}

  		virtual size_t getIdLength() const {
			return sizeof(ID_TYPE);
		}

		virtual bool needThresholding() const {
			return true;
		}

  		virtual void readId(void *unwarppedMarkerImgData, const size_t &unwarpedImageSize, void *dst) const {
  			int borderSizeInPixel = borderSizeInMeter * unwarpedImageSize / realSizeInMeter;
		    int cellSize = (unwarpedImageSize - borderSizeInPixel * 2.0) / MATRIX_SIZE;
		    int limit = cellSize*cellSize / 2;
		    int readIndex = 0;

		    ID_TYPE *id = (ID_TYPE *) dst;

		    BitMatrixMarkerImpl impl;
		    impl.loadImage(unwarpedImageSize, unwarppedMarkerImgData);

		    for (int i=0; i<MATRIX_SIZE; i++) {
		        for (int j=0; j<MATRIX_SIZE; j++) {
		            if (impl.countNonZero(
		            		j*cellSize+borderSizeInPixel,
		            		i*cellSize+borderSizeInPixel,
		            		(j+1)*cellSize-1+borderSizeInPixel,
		            		(i+1)*cellSize-1+borderSizeInPixel
	            		) > limit) {
		            	id->set(readIndex, true);
		            }
		            readIndex++;
		        }
		    }
  		}

  		virtual void detectFeatures(void *unwarpedMarkerImgData, const size_t &unwarpedImageSize, std::vector<double> &features) const {}
};

typedef BitMatrixMarker<2> BitMatrixMarker4;
typedef BitMatrixMarker<3> BitMatrixMarker9;
typedef BitMatrixMarker<4> BitMatrixMarker16;
typedef BitMatrixMarker<5> BitMatrixMarker25;
typedef BitMatrixMarker<6> BitMatrixMarker36;
typedef BitMatrixMarker<7> BitMatrixMarker49;
typedef BitMatrixMarker<8> BitMatrixMarker64;

#endif