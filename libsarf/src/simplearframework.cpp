#include "../include/simplearframework.h"

#include <memory>
#include <sstream>

#define UNWARPED_MAKER_IMG_SIZE 128
#define MARKER_POSE_HISTORY_MIN_SIZE 2
#define MARKER_POSE_HISTORY_MAX_SIZE 5

void Detector::init() {
    unwarpedCorners.at<cv::Point2f>(0, 0) = cv::Point2f(0, 0);
    unwarpedCorners.at<cv::Point2f>(1, 0) = cv::Point2f(0, UNWARPED_MAKER_IMG_SIZE);
    unwarpedCorners.at<cv::Point2f>(2, 0) = cv::Point2f(UNWARPED_MAKER_IMG_SIZE, UNWARPED_MAKER_IMG_SIZE);
    unwarpedCorners.at<cv::Point2f>(3, 0) = cv::Point2f(UNWARPED_MAKER_IMG_SIZE, 0);
}

Detector::Detector(const double *cameraMatrixValuesRowMajor, const double *distCoefsValues, PoseSolver *solver) {
    this->solver = solver;

    zAxis = cv::Vec4d(0, 0, 1, 1);
    markerPoints = cv::Mat(4, 1, CV_32FC2);
    unwarpedCorners = cv::Mat(4, 1, CV_32FC2);
    corners = cv::Mat(4, 1, CV_32FC2);

    setCameraMatrix(cameraMatrixValuesRowMajor);
    setDistCoefs(distCoefsValues);

    frameIndex = 0;

    init();
}

std::vector<ARMarker> Detector::findPotentialMarkers(const unsigned char *imageData, int width, int height) {
 	buffer.setBuffer(imageData, width, height);

    edgelDetector.setBuffer(&buffer);

    return edgelDetector.findMarkers();
}

void Detector::drawCorners(cv::Mat &dst, cv::Mat &corners) {
    cv::circle(dst, cv::Point(corners.at<cv::Vec2d>(0)), 5, CV_RGB(255, 0, 0), 1, 8);
    cv::circle(dst, cv::Point(corners.at<cv::Vec2d>(1)), 5, CV_RGB(0, 255, 0), 1, 8);
    cv::circle(dst, cv::Point(corners.at<cv::Vec2d>(2)), 5, CV_RGB(0, 0, 255), 1, 8);
    cv::circle(dst, cv::Point(corners.at<cv::Vec2d>(3)), 5, CV_RGB(255, 0, 255), 1, 8);
}



std::map<const MarkerDescription *, const MarkerPosition> Detector::matchKnownMarkers(std::vector<ARMarker> &potentialMarkers, cv::Mat image) {
	cv::Mat unwarpedMarker, thresholdedMarker, gray;
    cv::Matx33d mapMatrix;
    cv::Matx44d m1, m2;
    cv::Vec4d zAxisTransformed;
    float err1, err2;
    char id[4];
    int rotation;
    MarkerPosition position;
    int potentialMarkerIndex = 1;

    std::map<const MarkerDescription *, const MarkerPosition> result;

    markerPointsDouble = cv::Mat(4, 1, CV_64FC2, position.corners2d);

    cv::cvtColor(image, gray, CV_BGR2GRAY);

    for (auto &marker : potentialMarkers) {

        for (auto &pair : idCacheInfo) {
            pair.second = false;
        }

	    corners.at<cv::Point2f>(0) = cv::Point2f(marker.c1.x, marker.c1.y);
	    corners.at<cv::Point2f>(1) = cv::Point2f(marker.c2.x, marker.c2.y);
	    corners.at<cv::Point2f>(2) = cv::Point2f(marker.c3.x, marker.c3.y);
	    corners.at<cv::Point2f>(3) = cv::Point2f(marker.c4.x, marker.c4.y);

	    mapMatrix = cv::getPerspectiveTransform(corners, unwarpedCorners);
    	cv::warpPerspective(gray, unwarpedMarker, mapMatrix, cv::Size(UNWARPED_MAKER_IMG_SIZE, UNWARPED_MAKER_IMG_SIZE));

        thresholdedMarker.release();
        thresholdedMarker.data = NULL;

        int knowMarkerIndex = 0;

	    for (auto const &knownMarker : knownMarkers) {
            knowMarkerIndex++;
            std::type_index type = std::type_index(typeid(*knownMarker));

            if (!idCacheInfo[type]) {
                idCacheInfo[type] = true;
                auto &vector = idCache[type];
                std::fill(vector.begin(), vector.end(), 0);
                
                cv::Mat *markerImg = &unwarpedMarker;
                if (knownMarker->needThresholding()) {
                    if (thresholdedMarker.data == NULL) {
                        cv::threshold(unwarpedMarker, thresholdedMarker, 125, 255, cv::THRESH_BINARY | cv::THRESH_OTSU);
                    }
                    markerImg = &thresholdedMarker;
                }

                knownMarker->readId(
                    markerImg->data,
                    UNWARPED_MAKER_IMG_SIZE,
                    vector.data()
                );
            }

            rotation = knownMarker->isEqualsWithRotation(idCache[type].data(), knownMarker->getIdLength());

            if (rotation != -1) {
                for (; rotation > 0; rotation--) {
                    marker.rotateCornersClockwise();
                }

                markerPoints.at<cv::Vec2f>(0) = cv::Vec2f(marker.c1.x, marker.c1.y);
                markerPoints.at<cv::Vec2f>(1) = cv::Vec2f(marker.c2.x, marker.c2.y);
                markerPoints.at<cv::Vec2f>(2) = cv::Vec2f(marker.c3.x, marker.c3.y);
                markerPoints.at<cv::Vec2f>(3) = cv::Vec2f(marker.c4.x, marker.c4.y);

                cv::cornerSubPix(gray, markerPoints, cv::Size(5, 5), cv::Size(-1, -1), 
                    cv::TermCriteria( CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 40, 0.001 ));

                markerPoints.convertTo( markerPointsDouble, CV_64FC2 );

                if (solver->solveSquare(position, knownMarker->getRealSizeInMeter(), cameraMatrix.val, distCoefs.val)) {
                    result.insert(std::make_pair(knownMarker, position));
                }
                break;
            }
	    }

        potentialMarkerIndex++;
	}

    return result;
}

void Detector::registerMarker(const MarkerDescription *marker) {
    if (marker == NULL) {
        return;
    }
    std::type_index type = std::type_index(typeid(*marker));
	std::map<std::type_index, bool>::iterator it = idCacheInfo.find(type);
    if (it == idCacheInfo.end()) {
        idCacheInfo[type] = false;
        idCache[type] = std::vector<uint8_t>(marker->getIdLength(), (uint8_t) 0);
    }
    knownMarkers.push_back(marker);
}

std::map<const MarkerDescription*, const MarkerPosition> Detector::linearizePoses(
    std::map<const MarkerDescription*, const MarkerPosition> &currentFrameMarkers
) {
    std::map<const MarkerDescription*, const MarkerPosition> result;
    
    frameIndex++;

    //std::cout << "Frame index : " << frameIndex << std::endl;
    
    //For each detected markers in the current frame
    for (const auto &marker : currentFrameMarkers) {
        //Get back the pose history of this marker
        auto &detectedMarkerPoses = detectedMarkers[marker.first];

        //We only keep a defined number of poses.
        if (detectedMarkerPoses.size() == MARKER_POSE_HISTORY_MAX_SIZE) {
            detectedMarkerPoses.pop_front();
        }
        
        detectedMarkerPoses.push_back(std::make_pair(marker.second, frameIndex));
    }

    //For each marker already detected
    for (auto &marker : detectedMarkers) {

        auto &detectedMarkerPoses = marker.second;

        //If it history contains less than a certain number of pose, we do nothing.
        if (detectedMarkerPoses.size() < MARKER_POSE_HISTORY_MIN_SIZE) {
            continue;
        }

        int delta = frameIndex - detectedMarkerPoses.back().second;

        //If the current marker was not detected for 5 frame, we clear its history.
        if (delta > 10 || delta < 0) {
            detectedMarkerPoses.clear();
            continue;
        }

        double x = 0, y = 0, z = 0;

        for (const auto &positionHistory : detectedMarkerPoses) {
            x += positionHistory.first.transformation3d[3];
            y += positionHistory.first.transformation3d[7];
            z += positionHistory.first.transformation3d[11];
        }

        x /= detectedMarkerPoses.size();
        y /= detectedMarkerPoses.size();
        z /= detectedMarkerPoses.size();

        const MarkerPosition &lastPosition = detectedMarkerPoses.back().first;
        const double * lastPose = lastPosition.transformation3d;
        const double * lastDetection = lastPosition.corners2d;

        result.emplace(
            std::make_pair(
                marker.first,
                MarkerPosition{
                    lastPose[0], lastPose[1], lastPose[2],  x,
                    lastPose[4], lastPose[5], lastPose[6],  y,
                    lastPose[8], lastPose[9], lastPose[10], z,
                    lastPose[11], lastPose[12], lastPose[13], 1,
                    lastDetection[0], lastDetection[1],
                    lastDetection[2], lastDetection[3],
                    lastDetection[4], lastDetection[5],
                    lastDetection[6], lastDetection[7]
                }
            )
        );
    }

    return result;
}

std::map<const MarkerDescription*, const MarkerPosition> Detector::analyseImage(void *imageData, int width, int height) {
	std::vector<ARMarker> potentialMarkers = findPotentialMarkers((unsigned char *)imageData, width, height);
    cv::Mat image(cv::Size(width, height), CV_8UC3, imageData);
    return matchKnownMarkers(potentialMarkers, image);
}

std::map<const MarkerDescription*, const MarkerPosition> Detector::analyseImage(
        void *imageData, int width, int height, std::vector<ARMarker> &potentialMarkers
) {
    potentialMarkers = findPotentialMarkers((unsigned char *)imageData, width, height);
    cv::Mat image(cv::Size(width, height), CV_8UC3, imageData);
    return matchKnownMarkers(potentialMarkers, image);
}

std::map<const MarkerDescription*, const MarkerPosition> Detector::analyseImageLinear(
        void *imageData, int width, int height
) {
    std::vector<ARMarker> potentialMarkers = findPotentialMarkers((unsigned char *)imageData, width, height);
    cv::Mat image(cv::Size(width, height), CV_8UC3, imageData);
    std::map<const MarkerDescription*, const MarkerPosition> matchedMarkers = matchKnownMarkers(potentialMarkers, image);
    return linearizePoses(matchedMarkers);
}

std::map<const MarkerDescription*, const MarkerPosition> Detector::analyseImageLinear(
        void *imageData, int width, int height, std::vector<ARMarker> &potentialMarkers
) {
    potentialMarkers = findPotentialMarkers((unsigned char *)imageData, width, height);
    cv::Mat image(cv::Size(width, height), CV_8UC3, imageData);
    std::map<const MarkerDescription*, const MarkerPosition> matchedMarkers = matchKnownMarkers(potentialMarkers, image);
    return linearizePoses(matchedMarkers);
}

void Detector::setCameraMatrix(const double *values) {
    cameraMatrix = cv::Matx<double, 3, 3>(values);
}

void Detector::setDistCoefs(const double *values) {
    distCoefs = cv::Matx<double, 5, 1>(values);
}

