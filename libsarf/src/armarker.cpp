#include "armarker.h"

#include "linesegment.h"

void ARMarker::reconstructCorners() {
	c1 = chain[0].getIntersection( chain[1] );
	c2 = chain[1].getIntersection( chain[2] );
	
	if( chain.size() == 4 ) {
		c3 = chain[2].getIntersection( chain[3] );
		c4 = chain[3].getIntersection( chain[0] );
	} else {
		c3 = chain[2].end.position;
		c4 = chain[0].start.position;
	}

	//orderCornerClockwise();
}

void ARMarker::rotateCornersClockwise() {
	Vector2f tmp;
	tmp = c4;
	c4 = c1;
	c1 = c2;
	c2 = c3;
	c3 = tmp;
}

bool ARMarker::isRightOriented() {
	float l1 = c1.x*c1.x+c1.y*c1.y;
	float l2 = c2.x*c2.x+c2.y*c2.y;
	float l4 = c4.x*c4.x+c4.y*c4.y;

	return l1 <= l2 && l1 <= l4;
}

void ARMarker::orderCornerClockwise() {
	while (!isRightOriented()) {
		rotateCornersClockwise();
	}
}