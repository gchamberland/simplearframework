#include "../include/posesolver.h"

#include <opencv2/opencv.hpp>

#include "ippe.h"

IPPE::PoseSolver solver;
cv::Mat markerPoints;

bool IPPESquarePoseSolver::solveSquare(MarkerPosition &position, const float &squareSize, double *cameraMatrix, double *distCoefs) {
	cv::Matx44d m1, m2;
	float err1, err2;
	cv::Mat cameraMat(3, 3, CV_64F, cameraMatrix);
	cv::Mat distCoefsMat(5, 1, CV_64F, distCoefs);

	markerPoints = cv::Mat(1, 4, CV_64FC2, position.corners2d);

	solver.solveSquare(squareSize, markerPoints, cameraMat, distCoefsMat, m1, err1, m2, err2);

	if (err1 > 1.0) {
        return false;
    }

    cv::Vec4d zAxis(0, 0, 1, 0);
    cv::Vec4d zAxisTransformed = m1 * zAxis;
    
    double norm = std::sqrt(
        zAxisTransformed(0) * zAxisTransformed(0) +
        zAxisTransformed(1) * zAxisTransformed(1) +
        zAxisTransformed(2) * zAxisTransformed(2) +
        zAxisTransformed(3) * zAxisTransformed(3)
    );

    cv::Vec4d v1(zAxisTransformed(0) / norm, zAxisTransformed(1) / norm, zAxisTransformed(2) / norm, zAxisTransformed(3) / norm);

    double dot = v1.dot(zAxis);

    memcpy(
        &(position.transformation3d), 
        dot < 0 ? m1.val : m2.val,
        16 * sizeof(double)
    );

    return true;
}

bool IPPEGenericPoseSolver::solveSquare(MarkerPosition &position, const float &squareSize, double *cameraMatrix, double *distCoefs) {
    cv::Matx44d m1, m2;
    float err1, err2;
    cv::Mat cameraMat(3, 3, CV_64F, cameraMatrix);
    cv::Mat distCoefsMat(5, 1, CV_64F, distCoefs);

    markerPoints = cv::Mat(1, 4, CV_64FC2, position.corners2d);

    cv::Mat objectPoints;
    objectPoints.create(1, 4, CV_64FC3);
    solver.generateSquareObjectCorners3D(squareSize, objectPoints);
    solver.solveGeneric(objectPoints, markerPoints, cameraMat, distCoefsMat, m1, err1, m2, err2);

    if (err1 > 1.0) {
        return false;
    }

    cv::Vec4d zAxis(0, 0, 1, 0);
    cv::Vec4d zAxisTransformed = m1 * zAxis;
    
    double norm = std::sqrt(
        zAxisTransformed(0) * zAxisTransformed(0) +
        zAxisTransformed(1) * zAxisTransformed(1) +
        zAxisTransformed(2) * zAxisTransformed(2) +
        zAxisTransformed(3) * zAxisTransformed(3)
    );

    cv::Vec4d v1(zAxisTransformed(0) / norm, zAxisTransformed(1) / norm, zAxisTransformed(2) / norm, zAxisTransformed(3) / norm);

    double dot = v1.dot(zAxis);

    memcpy(
        &(position.transformation3d), 
        dot < 0 ? m1.val : m2.val,
        16 * sizeof(double)
    );

    return true;
}