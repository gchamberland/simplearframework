/**
 * 
 */

#ifndef _h_armarker_
#define _h_armarker_

#include "vector2f.h"
#include <vector>

class Edgel;
class LineSegment;

class ARMarker 
{
public:
	void reconstructCorners();
	void rotateCornersClockwise();
	void orderCornerClockwise();
	bool isRightOriented();
	
	std::vector<LineSegment> chain;
	Vector2f c1, c2, c3, c4;
};

#endif