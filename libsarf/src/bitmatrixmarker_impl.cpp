#include "../include/private/bitmatrixmarker_impl.h"

void BitMatrixMarkerImpl::loadImage(const size_t &imageSize, void *imageData) {
    img = cv::Mat(cv::Size(imageSize, imageSize), CV_8UC1, imageData);
}

int BitMatrixMarkerImpl::countNonZero(const size_t &x1, const size_t &y1, const size_t &x2, const size_t &y2) {
	cv::Rect roi(
    	cv::Point(x1, y1),
    	cv::Point(x2, y2)
	);
	return cv::countNonZero(img(roi));
}