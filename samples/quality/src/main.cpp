#include <opencv2/opencv.hpp>

#include <iostream>
#include <cmath>
#include <chrono>

#include <simplearframework.h>

void drawVector(float x, float y, float z, float r, float g, float b, const cv::Mat &pose, const cv::Mat &cameraMatrix, cv::Mat &dst) {
    //Origin = (0, 0, 0, 1)
    cv::Mat origin(4, 1, CV_64FC1, double(0));
    origin.at<double>(3, 0) = 1;

    //End = (x, y, z, 1)
    cv::Mat end(4, 1, CV_64FC1, double(1));
    end.at<double>(0, 0) = x; end.at<double>(1, 0) = y; end.at<double>(2, 0) = z;

    //multiply transformation matrix by camera matrix
    cv::Mat mat = cameraMatrix * pose.rowRange(0, 3);

    //project points
    origin = mat * origin;
    origin /= origin.at<double>(2, 0);
    end = mat * end;
    end /= end.at<double>(2, 0);
    
    //draw corresponding line
    cv::line(dst, cv::Point(origin.at<double>(0, 0), origin.at<double>(1, 0)), cv::Point(end.at<double>(0, 0), end.at<double>(1, 0)), CV_RGB(255 * r, 255 * g, 255 * b), 1); 
}

void drawAxis(const cv::Mat &pose, const cv::Mat &cameraMatrix, cv::Mat &dst, float size, float intensity = 1.0f) {
    drawVector(size, 0, 0, 1.0f * intensity, 0, 0, pose, cameraMatrix, dst);
    drawVector(0, size, 0, 0, 1.0f * intensity, 0, pose, cameraMatrix, dst);
    drawVector(0, 0, size, 0, 0, 1.0f * intensity, pose, cameraMatrix, dst);
}

void drawBorders(cv::Mat &dst, const ARMarker &marker, float r, float g, float b) {
    cv::line(dst, cv::Point(marker.c1.x, marker.c1.y), cv::Point(marker.c2.x, marker.c2.y), CV_RGB(255 * r, 255 * g, 255 * b), 1); 
    cv::line(dst, cv::Point(marker.c2.x, marker.c2.y), cv::Point(marker.c3.x, marker.c3.y), CV_RGB(255 * r, 255 * g, 255 * b), 1); 
    cv::line(dst, cv::Point(marker.c3.x, marker.c3.y), cv::Point(marker.c4.x, marker.c4.y), CV_RGB(255 * r, 255 * g, 255 * b), 1); 
    cv::line(dst, cv::Point(marker.c4.x, marker.c4.y), cv::Point(marker.c1.x, marker.c1.y), CV_RGB(255 * r, 255 * g, 255 * b), 1); 
}

void drawCorners(cv::Mat &dst, const cv::Mat &corners) {
    cv::circle(dst, cv::Point(corners.at<cv::Vec2d>(0)), 5, CV_RGB(255, 0, 0), 1, 8);
    cv::circle(dst, cv::Point(corners.at<cv::Vec2d>(1)), 5, CV_RGB(0, 255, 0), 1, 8);
    cv::circle(dst, cv::Point(corners.at<cv::Vec2d>(2)), 5, CV_RGB(0, 0, 255), 1, 8);
    cv::circle(dst, cv::Point(corners.at<cv::Vec2d>(3)), 5, CV_RGB(255, 0, 255), 1, 8);
}

double computeDistance(const MarkerPosition &m1, const MarkerPosition &m2) {
    const double *pose1 = m1.transformation3d, *pose2 = m2.transformation3d;
    const double &p1x = pose1[3], &p1y = pose1[7], &p1z = pose1[11],
        &p2x = pose2[3], &p2y = pose2[7], &p2z = pose2[11];

    double vx = p1x - p2x;
    double vy = p1y - p2y;
    double vz = p1z - p2z;

    return sqrt(vx*vx + vy*vy + vz*vz);
}

//Define a shortcut.
using Time = std::chrono::time_point<std::chrono::system_clock>;
using Duration = std::chrono::duration<double>;
using Clock = std::chrono::system_clock;

int main(int, char**)
{
    cv::VideoCapture cap("data/sample.mp4"); // open the default camera
    if(!cap.isOpened())  // check if we succeeded
        return -1;

    double scale = 1280.0 / 640.0;

    cv::Mat cameraMatrix(3, 3, CV_64FC1);
    cameraMatrix.setTo(0);
    cameraMatrix.at<double>(0, 0) = 3.9482411920291509e+002 * scale;
    cameraMatrix.at<double>(1, 1) = 3.9482411920291509e+002 * scale;
    cameraMatrix.at<double>(0, 2) = 320.0 * scale;
    cameraMatrix.at<double>(1, 2) = 240.0 * scale;
    cameraMatrix.at<double>(2, 2) = 1;

    cv::Mat distCoefs(5, 1, CV_64FC1);
    distCoefs.setTo(0);
    distCoefs.at<double>(0) = -1.6672041176452917e-002;
    distCoefs.at<double>(1) = -2.2670097318024499e-001;
    distCoefs.at<double>(2) = 0.;
    distCoefs.at<double>(3) = 0.;
    distCoefs.at<double>(4) = 1.0882744175809507e+000;

    cv::Vec4d zAxis(0, 0, 1, 1);

    Detector detector((const double *)cameraMatrix.data, (const double *)distCoefs.data);

    BitMatrixMarker4 marker1("1110", 0.02, 0.005);
    BitMatrixMarker64 marker2("1111111111111111111111111111111111111111111111111111111111111110", 0.05, 0.005);

    detector.registerMarker(&marker1);
    detector.registerMarker(&marker2);

    unsigned int nbFrame = 0;
    unsigned int potentialMarkerFound = 0;
    unsigned int falsePositive = 0;
    unsigned int recognizedMarker = 0;
    std::vector<double> computedDistances;
    std::vector<double> durations;


    for(;;)
    {
        cv::Mat frame;
        cap >> frame;
        if (!frame.data) {
            break;
        }
        cv::flip(frame, frame, 1);
        //cv::blur(frame, frame, cv::Size(5, 5));
        //cv::medianBlur(frame, frame, 5);

        std::vector<ARMarker> potentialMarkers;


        Time start = Clock::now();
        std::map<const MarkerDescription *, const MarkerPosition> foundMarkers =
            detector.analyseImage(frame.data, frame.cols, frame.rows, potentialMarkers);
        Time end = Clock::now();

        double duration = Duration(end - start).count();

        durations.push_back(duration);

        std::cout << "Analysis duration for this frame in second : " << duration << std::endl;

        for (auto const &marker : potentialMarkers) {
            drawBorders(frame, marker, 1, 0, 0);
            potentialMarkerFound++;
        }

        for (auto const &pair : foundMarkers) {
            //std::cout << *(pair.first) << std::endl;
            cv::Matx44d pose(pair.second.transformation3d);
            cv::Matx<double, 4, 2> corners(pair.second.corners2d);
            drawAxis(cv::Mat(pose), cv::Mat(cameraMatrix), frame, pair.first->getRealSizeInMeter() / 2.0, 0.5);
            recognizedMarker++;
        }

        if (foundMarkers.size() == 2) {
            double distance = computeDistance(foundMarkers[&marker1], foundMarkers[&marker2]);
            computedDistances.push_back(distance);
            std::cout << "Distance : " << distance << std::endl;
        }

        falsePositive += potentialMarkers.size() - foundMarkers.size();

        cv::imshow("test", frame);

        if(cv::waitKey(1) == 'q') break;

        nbFrame++;
    }

    double percentageOfRecognition = (recognizedMarker / 2.0) / (double) nbFrame;
    double avgComputedDistance = 0;

    for (const double &computedDistance : computedDistances) {
        avgComputedDistance += computedDistance;
    }

    avgComputedDistance /= computedDistances.size();

    double avgError = 0;
    double maxError = 0;
    double minError = 1.0e+100;
    for (const double &computedDistance : computedDistances) {
        double currentError = std::abs(computedDistance - 0.083);
        avgError += currentError;
        maxError = std::max(currentError, maxError);
        minError = std::min(currentError, minError);
    }

    avgError /= computedDistances.size();

    double avgDuration = 0;
    double maxDuration = 0;
    double minDuration = 1.0e+100;
    for (const double &duration : durations) {
        avgDuration += duration;
        maxDuration = std::max(duration, maxDuration);
        minDuration = std::min(duration, minDuration);
    }

    avgDuration /= durations.size();

    std::cout << "---------Detection----------" << std::endl;
    std::cout << "Number of frame analysed : " << nbFrame << std::endl;
    std::cout << "Number of marker detection : " << potentialMarkerFound << std::endl;
    std::cout << "Number of marker detection by frame (should be 2) : " << ((double) potentialMarkerFound / (double) nbFrame) << std::endl;
    std::cout << "Number of false positive detection : " << falsePositive << std::endl;
    std::cout << "False positive by frame : " << ((double) falsePositive / (double) nbFrame) << std::endl;
    std::cout << "Number of recognized marker : " << recognizedMarker << std::endl;
    std::cout << "Number of recognized marker by frame (should be 2) : " << ((double) recognizedMarker / (double) nbFrame) << std::endl;
    std::cout << "Recognition rate (Should be 1) : " << ((recognizedMarker / 2.0) / (double) nbFrame) << std::endl;
    std::cout << "-------Pose estimation------" << std::endl;
    std::cout << "Two markers where recognized in " << computedDistances.size() << " frame(s)" << std::endl;
    std::cout << "Average computed distance between two markers in meter (Should be 0.083) : " << avgComputedDistance << std::endl;
    std::cout << "Average error in meter : " << avgError << std::endl;
    std::cout << "Max. error in meter : " << maxError << std::endl;
    std::cout << "Min. error in meter : " << minError << std::endl;
    std::cout << "---------Performance--------" << std::endl;
    std::cout << "Average frame analysis duration in second : " << avgDuration << std::endl;
    std::cout << "Max. frame analysis duration : " << maxDuration << std::endl;
    std::cout << "Min. frame analysis duration : " << minDuration << std::endl;

    // the camera will be deinitialized automatically in VideoCapture destructor
    return 0;
}