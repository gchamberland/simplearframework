#include <opencv2/opencv.hpp>

#include <iostream>
#include <cmath>

#include <simplearframework.h>

void drawVector(float x, float y, float z, float r, float g, float b, const cv::Mat &pose, const cv::Mat &cameraMatrix, cv::Mat &dst) {
    //Origin = (0, 0, 0, 1)
    cv::Mat origin(4, 1, CV_64FC1, double(0));
    origin.at<double>(3, 0) = 1;

    //End = (x, y, z, 1)
    cv::Mat end(4, 1, CV_64FC1, double(1));
    end.at<double>(0, 0) = x; end.at<double>(1, 0) = y; end.at<double>(2, 0) = z;

    //multiply transformation matrix by camera matrix
    cv::Mat mat = cameraMatrix * pose.rowRange(0, 3);

    //project points
    origin = mat * origin;
    origin /= origin.at<double>(2, 0);
    end = mat * end;
    end /= end.at<double>(2, 0);
    
    //draw corresponding line
    cv::line(dst, cv::Point(origin.at<double>(0, 0), origin.at<double>(1, 0)), cv::Point(end.at<double>(0, 0), end.at<double>(1, 0)), CV_RGB(255 * r, 255 * g, 255 * b), 1); 
}

void drawAxis(const cv::Mat &pose, const cv::Mat &cameraMatrix, cv::Mat &dst, float size, float intensity = 1.0f) {
    drawVector(size, 0, 0, 1.0f * intensity, 0, 0, pose, cameraMatrix, dst);
    drawVector(0, size, 0, 0, 1.0f * intensity, 0, pose, cameraMatrix, dst);
    drawVector(0, 0, size, 0, 0, 1.0f * intensity, pose, cameraMatrix, dst);
}

void drawBorders(cv::Mat &dst, const ARMarker &marker, float r, float g, float b) {
    cv::line(dst, cv::Point(marker.c1.x, marker.c1.y), cv::Point(marker.c2.x, marker.c2.y), CV_RGB(255 * r, 255 * g, 255 * b), 1); 
    cv::line(dst, cv::Point(marker.c2.x, marker.c2.y), cv::Point(marker.c3.x, marker.c3.y), CV_RGB(255 * r, 255 * g, 255 * b), 1); 
    cv::line(dst, cv::Point(marker.c3.x, marker.c3.y), cv::Point(marker.c4.x, marker.c4.y), CV_RGB(255 * r, 255 * g, 255 * b), 1); 
    cv::line(dst, cv::Point(marker.c4.x, marker.c4.y), cv::Point(marker.c1.x, marker.c1.y), CV_RGB(255 * r, 255 * g, 255 * b), 1); 
}

void drawCorners(cv::Mat &dst, const cv::Mat &corners) {
    cv::circle(dst, cv::Point(corners.at<cv::Vec2d>(0)), 5, CV_RGB(255, 0, 0), 1, 8);
    cv::circle(dst, cv::Point(corners.at<cv::Vec2d>(1)), 5, CV_RGB(0, 255, 0), 1, 8);
    cv::circle(dst, cv::Point(corners.at<cv::Vec2d>(2)), 5, CV_RGB(0, 0, 255), 1, 8);
    cv::circle(dst, cv::Point(corners.at<cv::Vec2d>(3)), 5, CV_RGB(255, 0, 255), 1, 8);
}

double computeDistance(const MarkerPosition &m1, const MarkerPosition &m2) {
    const double *pose1 = m1.transformation3d, *pose2 = m2.transformation3d;
    const double &p1x = pose1[3], &p1y = pose1[7], &p1z = pose1[11],
        &p2x = pose2[3], &p2y = pose2[7], &p2z = pose2[11];

    double vx = p1x - p2x;
    double vy = p1y - p2y;
    double vz = p1z - p2z;

    return sqrt(vx*vx + vy*vy + vz*vz);
}

int main(int, char**)
{
    cv::VideoCapture cap(0); // open the default camera
    if(!cap.isOpened())  // check if we succeeded
        return -1;

    cv::Mat cameraMatrix(3, 3, CV_64FC1);
    cameraMatrix.setTo(0);
    cameraMatrix.at<double>(0, 0) = 4.5101367732454361e+002;
    cameraMatrix.at<double>(1, 1) = 4.5101367732454361e+002;
    cameraMatrix.at<double>(0, 2) = 320.0;
    cameraMatrix.at<double>(1, 2) = 240.0;
    cameraMatrix.at<double>(2, 2) = 1;

    cv::Mat distCoefs(5, 1, CV_64FC1);
    distCoefs.setTo(0);
    distCoefs.at<double>(0) = -2.3027581731418369e-002;
    distCoefs.at<double>(1) = -8.3412366043267305e-001;
    distCoefs.at<double>(2) = 0.;
    distCoefs.at<double>(3) = 0.;
    distCoefs.at<double>(4) = 4.7523755591537018e+000;

    cv::Vec4d zAxis(0, 0, 1, 1);

    Detector detector((const double *)cameraMatrix.data, (const double *)distCoefs.data);

    BitMatrixMarker4 marker1("1110", 0.02, 0.005);
    BitMatrixMarker64 marker2("1111111111111111111111111111111111111111111111111111111111111110", 0.05, 0.005);

    detector.registerMarker(&marker1);
    detector.registerMarker(&marker2);

    for(;;)
    {
        cv::Mat frame;
        cap >> frame;
        if (!frame.data) {
            continue;
        }
        cv::flip(frame, frame, 1);
        //cv::blur(frame, frame, cv::Size(5, 5));
        //cv::medianBlur(frame, frame, 5);

        std::vector<ARMarker> potentialMarkers;

        std::map<const MarkerDescription *, const MarkerPosition> foundMarkers =
            detector.analyseImage(frame.data, frame.cols, frame.rows, potentialMarkers);

        for (auto const &marker : potentialMarkers) {
            drawBorders(frame, marker, 1, 0, 0);
        }

        for (auto const &pair : foundMarkers) {
            //std::cout << *(pair.first) << std::endl;
            cv::Matx44d pose(pair.second.transformation3d);
            cv::Matx<double, 4, 2> corners(pair.second.corners2d);
            drawAxis(cv::Mat(pose), cv::Mat(cameraMatrix), frame, pair.first->getRealSizeInMeter() / 2.0, 0.5);
        }

        if (foundMarkers.size() == 2) {
            double distance = computeDistance(foundMarkers[&marker1], foundMarkers[&marker2]);
            std::cout << "Distance : " << distance << std::endl;
        }

        cv::imshow("test", frame);

        if(cv::waitKey(30) == 'q') break;
    }
    // the camera will be deinitialized automatically in VideoCapture destructor
    return 0;
}