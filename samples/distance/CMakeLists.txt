cmake_minimum_required(VERSION 3.2 FATAL_ERROR)

project(distance VERSION 0.1.0 LANGUAGES CXX C)

file(GLOB distance_SRC "src/*.cpp")

add_executable(distance ${distance_SRC})

target_link_libraries(distance PRIVATE sarf ${OpenCV_LIBS})