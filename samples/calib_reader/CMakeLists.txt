cmake_minimum_required(VERSION 3.2 FATAL_ERROR)

project(calib_reader VERSION 0.1.0 LANGUAGES CXX C)

file(GLOB calib_reader_SRC "src/*.cpp")

add_executable(calib_reader ${calib_reader_SRC})