#include <iostream>
#include <cstdio>
#include <iomanip>

static void swapDoubleBytes(void *v)
{
    char in[8], out[8];
    memcpy(in, v, 8);
    out[0] = in[7];
    out[1] = in[6];
    out[2] = in[5];
    out[3] = in[4];
    out[4] = in[3];
    out[5] = in[2];
    out[6] = in[1];
    out[7] = in[0];
    memcpy(v, out, 8);
}

bool isLittleEndian()
{
    short int number = 0x1;
    char *numPtr = (char*)&number;
    return (numPtr[0] == 1);
}

void invertMatrix4x4(double *inv, double *m) {
	int i;
	double det;

    inv[0] = m[5]  * m[10] * m[15] - 
             m[5]  * m[11] * m[14] - 
             m[9]  * m[6]  * m[15] + 
             m[9]  * m[7]  * m[14] +
             m[13] * m[6]  * m[11] - 
             m[13] * m[7]  * m[10];

    inv[4] = -m[4]  * m[10] * m[15] + 
              m[4]  * m[11] * m[14] + 
              m[8]  * m[6]  * m[15] - 
              m[8]  * m[7]  * m[14] - 
              m[12] * m[6]  * m[11] + 
              m[12] * m[7]  * m[10];

    inv[8] = m[4]  * m[9] * m[15] - 
             m[4]  * m[11] * m[13] - 
             m[8]  * m[5] * m[15] + 
             m[8]  * m[7] * m[13] + 
             m[12] * m[5] * m[11] - 
             m[12] * m[7] * m[9];

    inv[12] = -m[4]  * m[9] * m[14] + 
               m[4]  * m[10] * m[13] +
               m[8]  * m[5] * m[14] - 
               m[8]  * m[6] * m[13] - 
               m[12] * m[5] * m[10] + 
               m[12] * m[6] * m[9];

    inv[1] = -m[1]  * m[10] * m[15] + 
              m[1]  * m[11] * m[14] + 
              m[9]  * m[2] * m[15] - 
              m[9]  * m[3] * m[14] - 
              m[13] * m[2] * m[11] + 
              m[13] * m[3] * m[10];

    inv[5] = m[0]  * m[10] * m[15] - 
             m[0]  * m[11] * m[14] - 
             m[8]  * m[2] * m[15] + 
             m[8]  * m[3] * m[14] + 
             m[12] * m[2] * m[11] - 
             m[12] * m[3] * m[10];

    inv[9] = -m[0]  * m[9] * m[15] + 
              m[0]  * m[11] * m[13] + 
              m[8]  * m[1] * m[15] - 
              m[8]  * m[3] * m[13] - 
              m[12] * m[1] * m[11] + 
              m[12] * m[3] * m[9];

    inv[13] = m[0]  * m[9] * m[14] - 
              m[0]  * m[10] * m[13] - 
              m[8]  * m[1] * m[14] + 
              m[8]  * m[2] * m[13] + 
              m[12] * m[1] * m[10] - 
              m[12] * m[2] * m[9];

    inv[2] = m[1]  * m[6] * m[15] - 
             m[1]  * m[7] * m[14] - 
             m[5]  * m[2] * m[15] + 
             m[5]  * m[3] * m[14] + 
             m[13] * m[2] * m[7] - 
             m[13] * m[3] * m[6];

    inv[6] = -m[0]  * m[6] * m[15] + 
              m[0]  * m[7] * m[14] + 
              m[4]  * m[2] * m[15] - 
              m[4]  * m[3] * m[14] - 
              m[12] * m[2] * m[7] + 
              m[12] * m[3] * m[6];

    inv[10] = m[0]  * m[5] * m[15] - 
              m[0]  * m[7] * m[13] - 
              m[4]  * m[1] * m[15] + 
              m[4]  * m[3] * m[13] + 
              m[12] * m[1] * m[7] - 
              m[12] * m[3] * m[5];

    inv[14] = -m[0]  * m[5] * m[14] + 
               m[0]  * m[6] * m[13] + 
               m[4]  * m[1] * m[14] - 
               m[4]  * m[2] * m[13] - 
               m[12] * m[1] * m[6] + 
               m[12] * m[2] * m[5];

    inv[3] = -m[1] * m[6] * m[11] + 
              m[1] * m[7] * m[10] + 
              m[5] * m[2] * m[11] - 
              m[5] * m[3] * m[10] - 
              m[9] * m[2] * m[7] + 
              m[9] * m[3] * m[6];

    inv[7] = m[0] * m[6] * m[11] - 
             m[0] * m[7] * m[10] - 
             m[4] * m[2] * m[11] + 
             m[4] * m[3] * m[10] + 
             m[8] * m[2] * m[7] - 
             m[8] * m[3] * m[6];

    inv[11] = -m[0] * m[5] * m[11] + 
               m[0] * m[7] * m[9] + 
               m[4] * m[1] * m[11] - 
               m[4] * m[3] * m[9] - 
               m[8] * m[1] * m[7] + 
               m[8] * m[3] * m[5];

    inv[15] = m[0] * m[5] * m[10] - 
              m[0] * m[6] * m[9] - 
              m[4] * m[1] * m[10] + 
              m[4] * m[2] * m[9] + 
              m[8] * m[1] * m[6] - 
              m[8] * m[2] * m[5];

    det = m[0] * inv[0] + m[1] * inv[4] + m[2] * inv[8] + m[3] * inv[12];

    if (det == 0) {
    	throw std::runtime_error("Error : Matrix is not invertible."); 
    }

    det = 1.0 / det;

    for (i = 0; i < 16; i++)
        inv[i] = inv[i] * det;
}

void mulMatrix4x4(double *result, double *lhs, double *rhs) {
	double a = lhs[0], b = lhs[1], c = lhs[2], d = lhs[3];
	
	result[0] = a*rhs[0] + b*rhs[4] + c*rhs[8]  + d*rhs[12];
	result[1] = a*rhs[1] + b*rhs[5] + c*rhs[9]  + d*rhs[13];
	result[2] = a*rhs[2] + b*rhs[6] + c*rhs[10] + d*rhs[14];
	result[3] = a*rhs[3] + b*rhs[7] + c*rhs[11] + d*rhs[15];

	a = lhs[4]; b = lhs[5]; c = lhs[6]; d = lhs[7];

	result[4] = a*rhs[0] + b*rhs[4] + c*rhs[8]  + d*rhs[12];
	result[5] = a*rhs[1] + b*rhs[5] + c*rhs[9]  + d*rhs[13];
	result[6] = a*rhs[2] + b*rhs[6] + c*rhs[10] + d*rhs[14];
	result[7] = a*rhs[3] + b*rhs[7] + c*rhs[11] + d*rhs[15];

	a = lhs[8]; b = lhs[9]; c = lhs[10]; d = lhs[11];

	result[8]  = a*rhs[0] + b*rhs[4] + c*rhs[8]  + d*rhs[12];
	result[9]  = a*rhs[1] + b*rhs[5] + c*rhs[9]  + d*rhs[13];
	result[10] = a*rhs[2] + b*rhs[6] + c*rhs[10] + d*rhs[14];
	result[11] = a*rhs[3] + b*rhs[7] + c*rhs[11] + d*rhs[15];

	a = lhs[12]; b = lhs[13]; c = lhs[14]; d = lhs[15];

	result[12] = a*rhs[0] + b*rhs[4] + c*rhs[8]  + d*rhs[12];
	result[13] = a*rhs[1] + b*rhs[5] + c*rhs[9]  + d*rhs[13];
	result[14] = a*rhs[2] + b*rhs[6] + c*rhs[10] + d*rhs[14];
	result[15] = a*rhs[3] + b*rhs[7] + c*rhs[11] + d*rhs[15];
}

void genOrthoMatrix(double *dst, double ne, double fr, double right, double left, double top, double bottom) {
	dst[0 ] = 2.0 / (right - left);
	dst[1 ] = 0.0;
	dst[2 ] = 0.0;
	dst[3 ] = (right + left) / (left - right);
	dst[4 ] = 0.0;
	dst[5 ] = 2.0 / (top - bottom);
	dst[6 ] = 0.0;
	dst[7 ] = (top + bottom) / (bottom - top);
	dst[8 ] = 0.0;
	dst[9 ] = 0.0;
	dst[10] = 2.0 / (ne - fr);
	dst[11] = (fr + ne) / (ne - fr);
	dst[12] = 0.0;
	dst[13] = 0.0;
	dst[14] = 0.0;
	dst[15] = 1.0;
}

void printMatrix(double *matrix, const char *name, int precision = 20) {
	std::cout << std::setprecision(precision);
	std::cout << name << " : " << std::endl;
	std::cout << "\t" << matrix[0] <<
		"f,\t" << matrix[1] <<
		"f,\t" << matrix[2] <<
		"f,\t" << matrix[3] << "f," << std::endl;
	std::cout << "\t" << matrix[4] <<
		"f,\t" << matrix[5] <<
		"f,\t" << matrix[6] <<
		"f,\t" << matrix[7] << "f," << std::endl;
	std::cout << "\t" << matrix[8] <<
		"f,\t" << matrix[9] <<
		"f,\t" << matrix[10] <<
		"f,\t" << matrix[11] << "f," << std::endl;
	std::cout << "\t" << matrix[12] <<
		"f,\t" << matrix[13] <<
		"f,\t" << matrix[14] <<
		"f,\t" << matrix[15] << "f" << std::endl;
}


int main(int argc, char **argv) {
	
	if (argc != 2) {
		std::cout << "Usage : " << argv[0] << " file" << std::endl;
		return 0;
	}

	FILE *f = std::fopen(argv[1], "rb");

	if (f == NULL) {
		std::cout << "Cannot open file " << argv[1] << std::endl;
		return 0;
	}

	double calibrationMatrix[16], orthoMatrix[16], orthoMatrixInverse[16], readMatrix[16];

	if (std::fread(&readMatrix, sizeof(double), 16, f) != 16) {
		std::cout << "Error reading file" << std::endl;
		return 0;
	}

	if (isLittleEndian()) {
		std::cout << "Swapping values bytes because of endianess" << std::endl;
		for (int i=0; i<16; i++) {
			swapDoubleBytes(&(readMatrix[i]));
		}
	}

	printMatrix(readMatrix, "Should be the same that readMatrix");

	genOrthoMatrix(orthoMatrix, 0.1, 100.0, 960, 0, 540, 0);
	invertMatrix4x4(orthoMatrixInverse, orthoMatrix);
	mulMatrix4x4(calibrationMatrix, readMatrix, orthoMatrixInverse);

	printMatrix(calibrationMatrix, "Calibration matrix");

	genOrthoMatrix(orthoMatrix, 0.1, 100.0, 1280, 0, 720, 0);
	mulMatrix4x4(readMatrix, calibrationMatrix, orthoMatrix);

	printMatrix(readMatrix, "Changed ortho matrix");

	return 0;
}